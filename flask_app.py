# A very simple Flask Hello World app for you to get started with...

from flask import Flask, render_template, request
from flask_mail import Mail, Message
from flask_sslify import SSLify
import json


def get_value_with_key(key):
    return json.loads(open('credentials.json').read()).get(key)

app = Flask(__name__)

sslify = SSLify(app)

app.config['MAIL_SERVER'] = get_value_with_key('MAIL_SERVER')
app.config['MAIL_PORT'] = get_value_with_key('MAIL_PORT')
app.config['MAIL_USERNAME'] = get_value_with_key('MAIL_USERNAME')
app.config['MAIL_PASSWORD'] = get_value_with_key('MAIL_PASSWORD')
app.config['MAIL_USE_SSL'] = True
mail = Mail(app)


@app.route('/')
def hello_world():
    return render_template('index.html')


@app.route('/contact', methods=['POST'])
def send_contact():
    contact = request.get_json()
    print('contact: {}'.format(contact))
    msg = Message('Contact',
                  body=contact.get('message'),
                  sender='contact@didierfuentes.com',
                  recipients=['didier@didierfuentes.com'])
    mail.send(msg)
    return ''
